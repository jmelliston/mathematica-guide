# README #

My own introduction file to using Mathematica. It aims to cover a broad range of basics to give you a flavour for the language, including many of the vital things that you need to learn before embarking on using the xAct package.